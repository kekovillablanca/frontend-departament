import keycloackConfig from "./keycloack.config";

export const environment = {

  baseUrl : 'http://localhost:8080/',
  keycloak: keycloackConfig,
  production: false
};
//apiURL:"http://localhost:8080/",
