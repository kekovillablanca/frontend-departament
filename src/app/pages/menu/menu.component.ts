import { OnInit, Component } from '@angular/core';


import { MenuItem,PrimeIcons } from 'primeng/api';

import {KeycloakService} from "keycloak-angular";
import {MenubarModule} from "primeng/menubar";


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  imports: [
    MenubarModule
  ],

  standalone: true
})
export class MenuComponent implements OnInit {
  items: MenuItem[] | undefined;
  isLogged:boolean=false;
  constructor(protected serviceKeycloack: KeycloakService) {

  }
    async ngOnInit() {


      try {
        this.isLogged = await this.serviceKeycloack.isLoggedIn();
        console.log(this.isLogged);
      } catch (error) {
        console.error("Error al verificar el estado de inicio de sesión:", error);
      }
      this.items = [
        {
          label: 'File',
          icon: 'pi pi-fw pi-file',
          routerLink: ['/addresses']
        },
        {
          label: 'Edit',
          icon: 'pi pi-fw pi-pencil',
          routerLink: ['/departaments']
        },

        {
          label: 'Users',
          icon: 'pi pi-fw pi-user',
          items: [
            {
              label: 'New',
              icon: 'pi pi-fw pi-user-plus'
            },
            {
              label: 'Delete',
              icon: 'pi pi-fw pi-user-minus'
            },
            {
              label: 'Search',
              icon: 'pi pi-fw pi-users',
              items: [
                {
                  label: 'Filter',
                  icon: 'pi pi-fw pi-filter',
                  items: [
                    {
                      label: 'Print',
                      icon: 'pi pi-fw pi-print'
                    }
                  ]
                },
                {
                  icon: 'pi pi-fw pi-bars',
                  label: 'List'
                }
              ]
            }
          ]
        },
        {
          label: 'Inquilino',
          icon: 'pi pi-fw pi-calendar',
          routerLink:"/renters"
        },
        {

          label: this.isLogged ? "Deslogearse" : "Logearse",
          icon: "pi pi-user",
          command: () => {
          this.logUser();
          }
        },

      ];
    }


logUser() {
  if(this.isLogged) {
    this.serviceKeycloack.logout().then();

  }
  else {
    this.serviceKeycloack.login().then();

  }
}



}
