import { Component } from '@angular/core';

import {Address} from "../../../core/domain/Address";
import {Table, TableModule} from 'primeng/table';
import {MultiSelectModule} from "primeng/multiselect";
import {DropdownModule} from "primeng/dropdown";
import {TagModule} from "primeng/tag";
import {SliderModule} from "primeng/slider";
import {FormsModule} from "@angular/forms";
import {ToolbarModule} from "primeng/toolbar";
import {FileUploadModule} from "primeng/fileupload";
import {InputTextModule} from "primeng/inputtext";
import {Router} from "@angular/router";
import {DireccionService} from "../../../core/services/direccion/direccion.service";
@Component({
  selector: 'app-list-addresses',
  standalone: true,
  imports: [
    TableModule,
    MultiSelectModule,
    DropdownModule,
    TagModule,
    SliderModule,
    FormsModule,
    ToolbarModule,
    FileUploadModule,
    InputTextModule
  ],
  templateUrl: './list-addresses.component.html',
  styleUrl: './list-addresses.component.css'
})
export class ListAddressesComponent {
  addresses!: Address[];
  loading: boolean = true;


  statuses!: any[];



  activityValues: number[] = [0, 100];

  constructor(private direccionService: DireccionService,private routerAddress:Router) {}

  ngOnInit() {
    this.loading = true;
    this.direccionService.findAll().subscribe(d => {

      this.addresses = d;
      this.loading = false;
    })
  }
delete(address:Address){
this.direccionService.delete(address).subscribe(a=> console.log(a));

}


openNew(){
  this.routerAddress.navigate(['addresses', 'form']);
}


  clear(table: Table) {
    table.clear();
  }



}
