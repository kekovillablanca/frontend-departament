import { Component } from '@angular/core';
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";

@Component({
  selector: 'app-form-addresses',
  standalone: true,
  imports: [
    MatInputModule,
    MatSelectModule
  ],
  templateUrl: './form-addresses.component.html',
  styleUrl: './form-addresses.component.css'
})
export class FormAddressesComponent {

}
