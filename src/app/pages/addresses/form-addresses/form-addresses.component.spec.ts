import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddressesComponent } from './form-addresses.component';

describe('FormAddressesComponent', () => {
  let component: FormAddressesComponent;
  let fixture: ComponentFixture<FormAddressesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormAddressesComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
