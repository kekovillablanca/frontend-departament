import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAddressesComponent } from './detail-addresses.component';

describe('DetailAddressesComponent', () => {
  let component: DetailAddressesComponent;
  let fixture: ComponentFixture<DetailAddressesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DetailAddressesComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
