
import { Routes } from '@angular/router';
import {ListAddressesComponent} from "./list-addresses/list-addresses.component";
import {FormAddressesComponent} from "./form-addresses/form-addresses.component";



export const routes: Routes = [
 // { path: '', redirectTo: 'personas',pathMatch:"full" },
 { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: "list", component: ListAddressesComponent,pathMatch: 'full'},
  { path: 'form', component: FormAddressesComponent,pathMatch: 'full' }

];


