import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import {ToastModule} from "primeng/toast";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatSelect} from "@angular/material/select";
import {MatButton} from "@angular/material/button";
import {MatInput} from "@angular/material/input";
import {User} from "../../../core/domain/user";

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css'],
  providers: [MessageService],
  imports: [
    ToastModule,
    ReactiveFormsModule,
    MatFormField,
    MatLabel,
    MatSelect,
    MatButton,
    MatInput
  ],
  standalone: true
})
export class RegisterUserComponent implements OnInit {
  isUpdate: boolean = false;

  constructor(
    private routeNavigate: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) {}

  registroUsuario: FormGroup = this.fb.group({
    id: [null],
    email: [null, [Validators.required, Validators.min(1), Validators.email]],
    originName: [null, [Validators.required]],
  });
  hide = true;
  textRegister: string = 'Registrar usuario';
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.isUpdate = params.get('isUpdate') === 'true';
      });

  }


  showDialog(typeMsj: string): void {

  }

  private actualizarUsuario(): void {
    this.navigateToUsers();
  }
  navigateToUsers(): void {
    this.routeNavigate.navigate(['/usuarios/listar']);
  }

  public validateEmail(email: string): boolean {
    const emailPattern = /^[a-zA-Z0-9._-]+@unrn\.edu\.ar$/;
    return emailPattern.test(email);
  }



  saveUser(): void {

  }

}
