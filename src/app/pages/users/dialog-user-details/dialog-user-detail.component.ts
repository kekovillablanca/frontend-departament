import {Component, Inject, OnInit} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogTitle
} from '@angular/material/dialog';
import {MatButton} from "@angular/material/button";

@Component({
  selector: 'app-dialog-dependency-detail',
  templateUrl: './dialog-user-detail.component.html',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatButton,
    MatDialogActions,
    MatDialogClose
  ],
  styleUrls: ['./dialog-user-detail.component.css']
})
export class DialogUserDetailComponent implements OnInit {
  displayModal: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {

    console.log(data);
  }
  ngOnInit(): void {
    this.displayModal = true;
  }

  close() {
    this.displayModal = false;


  }
}
