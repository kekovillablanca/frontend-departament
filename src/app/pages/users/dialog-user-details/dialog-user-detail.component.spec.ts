import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDependencyDetailComponent } from './dialog-user-detail.component';

describe('DialogDependencyDetailComponent', () => {
  let component: DialogDependencyDetailComponent;
  let fixture: ComponentFixture<DialogDependencyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDependencyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDependencyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
