import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-dependency-detail',
  templateUrl: './dialog-user-detail.component.html',
  standalone: true,
  styleUrls: ['./dialog-user-detail.component.css']
})
export class DialogUserDetailComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
  }

  ngOnInit(): void {
    console.log(this.data);
  }

}
