import {navigation} from "../../shared/utils/navigate-util";
import {RegisterUserComponent} from "./register-user/register-user.component";
import {ListOfUserComponent} from "./list-of-user/list-of-user.component";

export const UsersRoutes = [
    {
        path: '',
        children: [
          {path: navigation.register, component: RegisterUserComponent },
          {path: navigation.update + navigation.id, component: RegisterUserComponent},
          {path: navigation.list, component: ListOfUserComponent},
          {path: navigation.paramList, component: ListOfUserComponent},
        ]
    }
]
