import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {DialogUserDetailComponent} from "../dialog-user-details/dialog-user-detail.component";
import {Table, TableModule} from "primeng/table";
import {ConfirmationService, ConfirmEventType, MessageService, PrimeNGConfig, SortEvent} from "primeng/api";
import {PaginatorModule, PaginatorState} from "primeng/paginator";
import {RoleFormatter} from "../../../shared/utils/role-formatter";
import {UserService} from "../../../core/services/user-service";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {ToastModule} from "primeng/toast";
import {MatIcon} from "@angular/material/icon";
import {MatIconButton} from "@angular/material/button";
import {DatePipe} from "@angular/common";
import {User} from "../../../core/domain/user";

@Component({
  selector: 'app-list-of-user',
  templateUrl: './list-of-user.component.html',
  styleUrls: ['./list-of-user.component.css'],
  standalone: true,
  imports: [
    TableModule,
    ConfirmDialogModule,
    PaginatorModule,
    ToastModule,
    MatIcon,
    MatIconButton,
    DatePipe
  ],
  providers: [ConfirmationService, MessageService]

})
export class ListOfUserComponent implements OnInit,AfterViewInit {
  selectedUser: any;
  totalRecords: any;
  loading: any;
  displayedColumns: string[] = [ 'Username', 'Fecha de Creación', 'Rol', 'Modificar'];
  users: User[] = [];
  numberOfRows: number = 20;
  private numberPage: number = 0;

  first = 0;
  protected last :any;
  constructor(private routeActivated: ActivatedRoute,
              private changeDetectorRef: ChangeDetectorRef,
              private primengConfig: PrimeNGConfig,
              private confirmationService: ConfirmationService,
              private messageService: MessageService,
              private roleFormatter: RoleFormatter,
              private service: UserService,
              public dialog: MatDialog,
              private route: Router) {
  }
  ngAfterViewInit():void{
    this.routeActivated.paramMap.subscribe((params: ParamMap) => {
      const title = params.get('title');
      if (title != null) {
        let message = params.get(`message`)?.toString();
        this.messageService.add({severity: 'success', summary: title, detail: message});
      }
    });
    this.changeDetectorRef.detectChanges();
  }
  ngOnInit(): void {
    this.loading = true;
    this.primengConfig.ripple = true;
    this.service.page(this.numberPage,this.numberOfRows).subscribe(page => {
      this.users = page.content;
      this.numberPage = page.pageable.pageNumber;
      this.last = page.size;
      this.totalRecords = page.totalElements;
      this.loading = false;
    });
  }
  showDialog(id: string): void {
    this.confirmationService.confirm({
      message: '¿Seguro que desea Eliminar al Usuario?',
      header: 'Confirmacion',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.deleteUser(id);
        this.messageService.add({severity:'success', summary:'Confirmed', detail:'Usuario eliminado'});
      },
      reject: (type: any) => {
        switch(type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({severity:'warn', summary:'Cancelar', detail:'Seleccionaste cancelar'});
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({severity:'warn', summary:'Cancelar', detail:'Seleccionaste cancelar'});
            break;
        }
      }
    });


  }

    deleteUser(id: string): void{
    let user = new User();
    user.id = id;
    this.service.delete(user).subscribe(response => {
      this.ngOnInit();
    });
  }

  customSort(event: SortEvent) {
  }
  compare(a: number | string, b: number | string, isAsc: boolean): any {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  redirectToUpdate(id: string): void {
    this.route.navigate(['sale-system/pages/users/register-params',
      id,
      true]).then();
  }

  redirectToRegister(): void{
    this.route.navigate(['sale-system/pages/users/register']).then();
  }
  redirectToDetails( user: User) {
    this.dialog.open(DialogUserDetailComponent, {
      data: {
        user: user,
        displayModal: true,
      },enterAnimationDuration:300,
      exitAnimationDuration:300,
      hasBackdrop:true,
    });
  }

  formatRole(value: string): String{
    return this.roleFormatter.formatRole(value);
  }





  clear(table: Table) {
    table.clear();
    this.ngOnInit();
  }



  customFilter(event: any, table: Table) {
    }


  pagingTable(event: PaginatorState, dt1: Table) {
    this.numberOfRows = <number> event.page;
    this.numberPage = <number> event.rows;
    dt1.clear();
    this.ngOnInit();
  }


}
