import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRentersComponent } from './form-renters.component';

describe('FormRentersComponent', () => {
  let component: FormRentersComponent;
  let fixture: ComponentFixture<FormRentersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormRentersComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormRentersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
