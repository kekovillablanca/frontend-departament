
import {  Routes } from '@angular/router';
import {ListRentersComponent} from "./list-renters/list-renters.component";
import {FormRentersComponent} from "./form-renters/form-renters.component";


export const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: "list", component: ListRentersComponent,pathMatch: 'full'},
  { path: 'form', component: FormRentersComponent,pathMatch: 'full' }
];

