import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRentersComponent } from './detail-renters.component';

describe('DetailRentersComponent', () => {
  let component: DetailRentersComponent;
  let fixture: ComponentFixture<DetailRentersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DetailRentersComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailRentersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
