import { Component } from '@angular/core';
import {ButtonModule} from "primeng/button";
import {FileUploadModule} from "primeng/fileupload";
import {InputTextModule} from "primeng/inputtext";
import {MultiSelectModule} from "primeng/multiselect";
import {SharedModule} from "primeng/api";
import {Table, TableModule} from "primeng/table";
import {ToolbarModule} from "primeng/toolbar";
import {Router} from "@angular/router";

import {Renter} from "../../../core/domain/Renter";
import {FormsModule} from "@angular/forms";
import {InquilinoService} from "../../../core/services/inquilino/inquilino.service";

@Component({
  selector: 'app-list-renters',
  standalone: true,
  templateUrl: './list-renters.component.html',
  imports: [
    ButtonModule,
    FileUploadModule,
    InputTextModule,
    MultiSelectModule,
    SharedModule,
    TableModule,
    ToolbarModule,
    FormsModule
  ],
  styleUrl: './list-renters.component.css'
})
export class ListRentersComponent {
  renters!: Renter[];
  loading: boolean = true;
  constructor(private inquilinoService: InquilinoService,private routerAddress:Router) {}

  ngOnInit() {
    this.loading = true;
    this.inquilinoService.findAll().subscribe(i => {
      console.log(i);
      this.renters = i;
      this.loading = false;
    })
  }



  openNew(){
    this.routerAddress.navigate(['renters', 'form']);
  }


  clear(table: Table) {
    table.clear();
  }
}
