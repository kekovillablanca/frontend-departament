import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRentersComponent } from './list-renters.component';

describe('ListRentersComponent', () => {
  let component: ListRentersComponent;
  let fixture: ComponentFixture<ListRentersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListRentersComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListRentersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
