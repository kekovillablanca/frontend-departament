import { Component } from '@angular/core';

import {Router} from "@angular/router";
import {Table, TableModule} from "primeng/table";


import {Departament} from "../../../core/domain/Departament";
import {ButtonModule} from "primeng/button";
import {FileUploadModule} from "primeng/fileupload";
import {InputTextModule} from "primeng/inputtext";
import {SharedModule} from "primeng/api";
import {ToolbarModule} from "primeng/toolbar";
import {DepartamentosService} from "../../../core/services/departamentos/departamentos.service";

@Component({
  selector: 'app-list-departaments',
  standalone: true,
  imports: [
    ButtonModule,
    FileUploadModule,
    InputTextModule,
    SharedModule,
    TableModule,
    ToolbarModule
  ],
  templateUrl: './list-departaments.component.html',
  styleUrl: './list-departaments.component.css'
})
export class ListDepartamentsComponent {
  departaments!: Departament[];
  loading: boolean = true;

  constructor(private departamentService: DepartamentosService,private routerAddress:Router) {}

  ngOnInit() {
    this.loading = true;
    this.departamentService.findAll().subscribe(d => {
      this.departaments = d;
      this.loading = false;
    })
  }



  openNew(){
    this.routerAddress.navigate(['departaments', 'form']);
  }


  clear(table: Table) {
    table.clear();
  }
}
