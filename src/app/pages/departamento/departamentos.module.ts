import {MessageService, SharedModule} from "primeng/api";
import {DepartamentosRoutingModule} from "./departamentos-routing.module";
import {AutoFocusModule} from "primeng/autofocus";
import {ContextMenuModule} from "primeng/contextmenu";
import {ChipModule} from "primeng/chip";
import {MenubarModule} from "primeng/menubar";
import {StepsModule} from "primeng/steps";
import {TreeModule} from "primeng/tree";
import {TableModule} from "primeng/table";
import {AvatarModule} from "primeng/avatar";
import {AvatarGroupModule} from "primeng/avatargroup";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AccordionModule} from "primeng/accordion";
import {AutoCompleteModule} from "primeng/autocomplete";
import {BadgeModule} from "primeng/badge";
import {BreadcrumbModule} from "primeng/breadcrumb";
import {BlockUIModule} from "primeng/blockui";
import {ButtonModule} from "primeng/button";
import {CalendarModule} from "primeng/calendar";
import {CarouselModule} from "primeng/carousel";
import {CascadeSelectModule} from "primeng/cascadeselect";
import {ChartModule} from "primeng/chart";
import {CheckboxModule} from "primeng/checkbox";
import {ChipsModule} from "primeng/chips";
import {ColorPickerModule} from "primeng/colorpicker";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {ConfirmPopupModule} from "primeng/confirmpopup";
import {VirtualScrollerModule} from "primeng/virtualscroller";
import {DataViewModule} from "primeng/dataview";
import {DialogModule} from "primeng/dialog";
import {DividerModule} from "primeng/divider";
import {DockModule} from "primeng/dock";
import {DragDropModule} from "primeng/dragdrop";
import {DropdownModule} from "primeng/dropdown";
import {DynamicDialogModule} from "primeng/dynamicdialog";
import {EditorModule} from "primeng/editor";
import {FieldsetModule} from "primeng/fieldset";
import {FileUploadModule} from "primeng/fileupload";
import {GalleriaModule} from "primeng/galleria";
import {InplaceModule} from "primeng/inplace";
import {InputMaskModule} from "primeng/inputmask";
import {InputSwitchModule} from "primeng/inputswitch";
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {InputNumberModule} from "primeng/inputnumber";
import {ImageModule} from "primeng/image";
import {KnobModule} from "primeng/knob";
import {ListboxModule} from "primeng/listbox";
import {MegaMenuModule} from "primeng/megamenu";
import {MenuModule} from "primeng/menu";
import {MessageModule} from "primeng/message";
import {MessagesModule} from "primeng/messages";
import {MultiSelectModule} from "primeng/multiselect";
import {OrganizationChartModule} from "primeng/organizationchart";
import {OrderListModule} from "primeng/orderlist";
import {OverlayPanelModule} from "primeng/overlaypanel";
import {PaginatorModule} from "primeng/paginator";
import {PanelModule} from "primeng/panel";
import {PanelMenuModule} from "primeng/panelmenu";
import {PasswordModule} from "primeng/password";
import {PickListModule} from "primeng/picklist";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ProgressBarModule} from "primeng/progressbar";
import {RadioButtonModule} from "primeng/radiobutton";
import {RatingModule} from "primeng/rating";
import {SelectButtonModule} from "primeng/selectbutton";
import {SidebarModule} from "primeng/sidebar";
import {ScrollerModule} from "primeng/scroller";
import {ScrollPanelModule} from "primeng/scrollpanel";
import {ScrollTopModule} from "primeng/scrolltop";
import {SkeletonModule} from "primeng/skeleton";
import {SlideMenuModule} from "primeng/slidemenu";
import {SliderModule} from "primeng/slider";
import {SpeedDialModule} from "primeng/speeddial";
import {SpinnerModule} from "primeng/spinner";
import {SplitterModule} from "primeng/splitter";
import {SplitButtonModule} from "primeng/splitbutton";
import {TabMenuModule} from "primeng/tabmenu";
import {TabViewModule} from "primeng/tabview";
import {TagModule} from "primeng/tag";
import {TerminalModule} from "primeng/terminal";
import {TieredMenuModule} from "primeng/tieredmenu";
import {TimelineModule} from "primeng/timeline";
import {ToastModule} from "primeng/toast";
import {ToggleButtonModule} from "primeng/togglebutton";
import {ToolbarModule} from "primeng/toolbar";
import {TooltipModule} from "primeng/tooltip";
import {TriStateCheckboxModule} from "primeng/tristatecheckbox";
import {TreeSelectModule} from "primeng/treeselect";
import {TreeTableModule} from "primeng/treetable";
import {AnimateModule} from "primeng/animate";
import {CardModule} from "primeng/card";
import {CommonModule} from "@angular/common";

import {NgModule} from "@angular/core";
import {ListDepartamentsComponent} from "./list-departaments/list-departaments.component";
import {FormDepartamentsComponent} from "./form-departaments/form-departaments.component";
import {DetailDepartamentsComponent} from "./detail-departaments/detail-departaments.component";



@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
    DepartamentosRoutingModule,
    AutoFocusModule,
    ContextMenuModule,
    ChipModule,
    MenubarModule,
    StepsModule,
    TreeModule,
    TableModule,
    AvatarModule,
    AvatarGroupModule,
    FormsModule,
    ReactiveFormsModule,
    AccordionModule,
    AutoCompleteModule,
    BadgeModule,
    BreadcrumbModule,
    BlockUIModule,
    ButtonModule,
    CalendarModule,
    CarouselModule,
    CascadeSelectModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    ColorPickerModule,
    ConfirmPopupModule,
    VirtualScrollerModule,
    DataViewModule,
    DialogModule,
    DividerModule,
    DockModule,
    DragDropModule,
    DropdownModule,
    DynamicDialogModule,
    EditorModule,
    FieldsetModule,
    FileUploadModule,
    GalleriaModule,
    InplaceModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    ImageModule,
    KnobModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OrganizationChartModule,
    OrderListModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    SelectButtonModule,
    SidebarModule,
    ScrollerModule,
    ScrollPanelModule,
    ScrollTopModule,
    SkeletonModule,
    SlideMenuModule,
    SliderModule,
    SpeedDialModule,
    SpinnerModule,
    SplitterModule,
    SplitButtonModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TagModule,
    TerminalModule,
    TieredMenuModule,
    TimelineModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TriStateCheckboxModule,
    TreeSelectModule,
    TreeTableModule,
    AnimateModule,
    CardModule,
    RatingModule,
    CommonModule,ConfirmDialogModule,
      ListDepartamentsComponent,
      FormDepartamentsComponent,
      DetailDepartamentsComponent
  ],

})

export class DepartamentosModule { }
