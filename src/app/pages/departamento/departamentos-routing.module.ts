import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import {ListDepartamentsComponent} from "./list-departaments/list-departaments.component";
import {ListAddressesComponent} from "../../pages/addresses/list-addresses/list-addresses.component";
import {FormAddressesComponent} from "../../pages/addresses/form-addresses/form-addresses.component";
import {FormDepartamentsComponent} from "./form-departaments/form-departaments.component";

export const routes: Routes = [
  // { path: '', redirectTo: 'personas',pathMatch:"full" },
  //{ path: '', redirectTo: '', pathMatch: 'full' },
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: "list", component: ListDepartamentsComponent,pathMatch: 'full'},
  { path: 'form', component: FormDepartamentsComponent,pathMatch: 'full' }
 ];


