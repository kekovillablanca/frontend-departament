import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDepartamentsComponent } from './form-departaments.component';

describe('FormDepartamentsComponent', () => {
  let component: FormDepartamentsComponent;
  let fixture: ComponentFixture<FormDepartamentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormDepartamentsComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormDepartamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
