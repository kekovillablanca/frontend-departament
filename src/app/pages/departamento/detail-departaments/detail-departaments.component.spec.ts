import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDepartamentsComponent } from './detail-departaments.component';

describe('DetailDepartamentsComponent', () => {
  let component: DetailDepartamentsComponent;
  let fixture: ComponentFixture<DetailDepartamentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DetailDepartamentsComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailDepartamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
