import { Routes } from '@angular/router';
import {navigation} from "./shared/utils/navigate-util";
import {RootingBrowserNavComponent} from "./shared/components/rooting-browser-nav/rooting-browser-nav.component";
import {AuthKeycloakGuard} from "./core/config/init/auth-keycloack.guard";
import {HomeComponent} from "./pages/menu/home/home.component";

export const routes: Routes = [
  {path: navigation.pages, canActivate: [AuthKeycloakGuard],
    component: RootingBrowserNavComponent, children: [
      {path: navigation.home, component: HomeComponent,},
      {path: navigation.users,
        loadChildren: () => import('./pages/users/users.routes').then(r => r.UsersRoutes)
      },

      {path: navigation.addresses,
        loadChildren: () => import('./pages/addresses/addresses-routing.module').then(r => r.routes)},

      {path: navigation.departments,
        loadChildren: () => import('./pages/departamento/departamentos-routing.module').then(r => r.routes)
      },
      {path: navigation.renters,
        loadChildren: () => import('./pages/renters/renter-routing.module').then(r => r.routes)},

    ]},
  {path: '', redirectTo: navigation.pages + "/" + navigation.home, pathMatch: 'full'},
  {path: '**', redirectTo: navigation.home, pathMatch: 'full'},


];

