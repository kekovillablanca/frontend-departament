import {Observable} from 'rxjs';

export interface  QueryService <T>{
  findAll(): Observable<T[]>;
 // get(id: string): Observable<T>;
  //page(page: number, rows: number): Observable<any>;
}

