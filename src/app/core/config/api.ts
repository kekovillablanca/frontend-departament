import {environment} from "../../../environments/environment";

const query = 'query-';
const command = 'command-';
const departments = 'departments';
const renters = 'renters';
const address = 'address';

export const AddresEndpoint = {
  post: environment.baseUrl + command + address,
  get: environment.baseUrl + query + address,

}
export const RentersEndpoint = {
  post: environment.baseUrl + command + renters,
  get: environment.baseUrl + query + renters,
}
export const DepartamentEndpoint = {
  post: environment.baseUrl + command + departments,
  get: environment.baseUrl + query + departments,
}



