import {KeycloakBearerInterceptor, KeycloakService} from 'keycloak-angular';
import {APP_INITIALIZER, Provider} from "@angular/core";
import {HTTP_INTERCEPTORS, HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {environment} from "../../../../environments/environment";

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise( (resolve, reject) => {
      try {
         keycloak.init({
           config: {
             url: environment.keycloak.url,
             realm: environment.keycloak.realm,
             clientId: environment.keycloak.clientId,
           },
           loadUserProfileAtStartUp: true,
           initOptions: {
             //onLoad: 'check-sso',
             onLoad: 'login-required',
             checkLoginIframe: false,
           },
           bearerExcludedUrls: ['/assets'],
           enableBearerInterceptor: true,
           bearerPrefix: 'Bearer ',
          }).then();
        resolve(resolve);
      } catch (error) {
        reject(error);
      }
    });
  };
}
// Provider for Keycloak Bearer Interceptor
export const KeycloakBearerInterceptorProvider: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: KeycloakBearerInterceptor,
  multi: true
};

// Provider for Keycloak Initialization
export const KeycloakInitializerProvider: Provider = {
  provide: APP_INITIALIZER,
  useFactory: initializer,
  multi: true,
  deps: [KeycloakService]
}
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http,'./assets/i18n/', '.json');
}
