
import { environment } from "src/environments/environment";
import { KeycloakService } from 'keycloak-angular';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: {
            url: environment.keycloak.url,
            realm: environment.keycloak.realm,
            clientId: environment.keycloak.clientId,
          },
          loadUserProfileAtStartUp: true,
          initOptions: {
            onLoad: 'check-sso',
            //onLoad: 'login-required',
            checkLoginIframe: false,
          },
          bearerExcludedUrls: ['/assets'],
          enableBearerInterceptor: true,
          bearerPrefix: 'Bearer ',
        });
        resolve(resolve);
      } catch (error) {
        reject(error);
      }
    });
  };
}
