import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

export  interface CommandService <T>{
  save(entity: T): Observable<T>;
  update(entity: T): Observable<T>;
  delete(entity: T): Observable<T>;
}

