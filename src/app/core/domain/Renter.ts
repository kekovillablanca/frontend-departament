export class Renter {
  id:string="";
  name:string="";
  dni:string="";
  lastname:string="";
  salary:number=0;
  phone:string="";
  constructor(id: string,name: string, lastname: string, dni: string,salary:number,phone:string) {
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.dni = dni;
    this.salary=salary;
    this.phone=phone;
  }



}
