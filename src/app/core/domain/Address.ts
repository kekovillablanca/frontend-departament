export class Address {

  id:string="";
  number:number= 0;
  neighborhood:string="";
  street:string="";

  constructor(id: string,street: string, neighborhood: string, number: number) {
    this.id = id;
    this.street = street;
    this.neighborhood = neighborhood;
    this.number = number;
  }

}
