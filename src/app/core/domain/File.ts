import {Departament} from "./Departament";

export class File{
    id:string="";
    departament!: Departament;
    extension:string="";
    name:string="";
    path:string="";
    url:string=""

    constructor(id:string,departament:Departament,extension:string,name:string,path:string,url:string) {
        this.id=id;
        this.departament=departament;
        this.extension=extension;
        this.name=name;
        this.path=path;
        this.url=url;
    }


}
