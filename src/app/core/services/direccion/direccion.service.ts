import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, catchError, map, throwError } from 'rxjs';
import { Address } from '../../domain/Address';
import {QueryService} from "../../query/query.service";
import {CommandService} from "../../command/command.service";
import {AddresEndpoint} from "../../config/api";

@Injectable({
  providedIn: 'root'
})
export class DireccionService implements QueryService<Address>,CommandService<Address> {

  constructor(private httpClient: HttpClient) {

  }

  public findAll(): Observable<Address[]> {
    return this.httpClient.get<Address[]>(AddresEndpoint.get).pipe(
      map(direcciones =>

          direcciones.map(d => new Address(d.id, d.street, d.neighborhood,d.number))
      )
    )
  }
  /*    return this.httpClient.get<Renter[]>(RentersEndpoint.get).pipe(
      map(inquilinos =>
          inquilinos.map(p => new Renter(p.id, p.name, p.lastname,p.dni,p.salary,p.phone))
      )
    )
  }*/

  public update(direccion: Address): Observable<Address> {
    return this.httpClient.put<Address>(AddresEndpoint.post + '/' + direccion.id, direccion);

  }
  public save(direccion: Address): Observable<Address> {
    return this.httpClient.post<Address>(AddresEndpoint.post, direccion).pipe(catchError(error => {
      return throwError("la direccion no existe..", error);
    }));

  }
  public delete(address: Address): Observable<Address>{
    return this.httpClient.delete<Address>(AddresEndpoint.post+ '/' + address.id);

  }
}
