/*
import {Injectable, OnInit} from "@angular/core";
import {AuthService} from "./auth-service";

export interface ProductData {
  description: string | null;
  brand: string | null;
}
@Injectable({
  providedIn: 'root'
})

export class VoiceRecognitionService implements OnInit{
  recognition: any;
  text: string = '';
  recognitionIsEnable!: boolean;
  constructor(
    private authService: AuthService,

  ) {
    }

  ngOnInit(): void {
    }

  init() {
    this.recognition.lang = 'es-ES'; // Establecemos el idioma en español
    this.recognition.addEventListener('result', (e: any) => {
      if(this.text === (this.getLastTranscript(e))){
        this.text = '';
      }else{
        this.text = this.getLastTranscript(e);
      }
      if(this.containsClue(this.text)){
        this.verifyThatRequired();
      }
      if(this.text.toLowerCase().replace(',','').replace('.','').includes('gracias evan')){
        this.speak('Siempre es un placer ' + this.authService.getName());
      }
      this.text = '';
    });

    this.recognition.onend = (event: any) => {
      console.log('entra aend');
      this.startRecognition();
    }

  }

  verifyThatRequired() {
    this.verifyIfWantKnowPrice();
  }

  verifyIfWantKnowPrice() {
    const wantedWords = ['cuál es el precio','cual es el precio','decime','cuanto sale','cuánto está', 'cuanto esta', 'cuanto cuesta', 'precio', 'valor', 'valor de','dime el precio de'];
    const text = this.text;
    const isWanted = wantedWords.some(word => text.includes(word));
    if(isWanted){
      this.findDescriptionOfText(text)
    }
  }

  findDescriptionOfText(text: string) {
    // Definir las palabras a reemplazar en un array
    const palabrasARemplazar = ['evan,', 'Evan,', 'Evàn', 'cuanto esta', 'cuànto esta', 'cuanto està', 'cuànto està'];
    // Crear una expresión regular que coincida con todas las palabras a reemplazar
    const regexPalabras = new RegExp(palabrasARemplazar.join('|'), 'gi');
    // Reemplazar todas las coincidencias con una cadena vacía
    text = text.replace(regexPalabras, '');

    const regexWithBrand =  /(?:el|la)\s+([\w\s]+)\s+(?:de\s+(?:marca|con\s+marca|de\s+marca) )(\S+)\?/i;
    const regexWithoutBrand = /(?:el|la)\s+([\w\s]+)\?/i;
    const matchWithBrand = text.match(regexWithBrand);
    const matchWithoutBrand = text.match(regexWithoutBrand);
    const data: ProductData = {
      description: '',
      brand: ''
    }
    if (matchWithBrand && matchWithBrand.length === 3) {
      console.log('entra');
      data.description = matchWithBrand[1];
      data.brand = matchWithBrand[2];
    }
    else if (matchWithoutBrand && matchWithoutBrand.length === 2) {
      data.description = matchWithoutBrand[1];
    }
     if (data.brand && data.description && matchWithBrand) {
      this.productService.findByBrandAndDescription(data.description, data.brand).subscribe((products: Product[]) => {
        this.speakIfProductsFound(products,data);
      }, () => {
      })
    }
    else if (data.description && matchWithoutBrand) {
      this.productService.findByDescription(data.description).subscribe((products: Product[]) => {
        this.products = products;
        this.speakIfProductsFound(products, data);

      }, () => {
      });
    }
    this.text = '';
  }

  describeTheBrand(description: string) {
    const regex = /(?:de la marca|marca|con marca| de marca)\s+(.*)/i;
    const match = description.match(regex);
    return (match && match.length > 1);
  }

  removeQuestionAndExclamationMarks(description: string): string {
    description = description.replace(/(\?|\!|\,|\.)/g, '');
    description = description.replace('por favor','') ;
    description = description.replace('porfavor','') ;
    description = description.replace('?','') ;
    description = description.replace('¿','') ;
    description = description.replace(',','') ;
    description = description.replace('gracias','') ;
    description = description.replace('si no es mucha molestia','') ;
    description = description.replace(' por favor','') ;
    return description.trim();
  }
  public containsClue(transcript: string){
    const clues = ['ey evan', 'evan', 'hola evan', '¿evan','evan,','evan?', 'Evan', 'Evan,','Evan?'];
    return clues.some(clue => transcript.includes(clue));
  }
  getLastTranscript(e: any) {
    const lastResultIndex = e.results.length - 1;
    return e.results[lastResultIndex][0].transcript.trim();
  }

  closeRecognition() {
    localStorage.setItem('searchByVoice', 'false');
    this.recognitionIsEnable = false;
    this.recognition.stop();
    window.location.reload();

  }
  startRecognition() {
    // @ts-ignore
    this.recognition = new webkitSpeechRecognition();
    this.recognition.continuous = true;
    this.init();
    localStorage.setItem('searchByVoice', 'true');
    this.recognitionIsEnable = true;
    this.recognition.start();

  }

  speak(textToSpeack: string){
    console.log(textToSpeack);
    let utterance = new SpeechSynthesisUtterance(textToSpeack);
    utterance.lang = 'es-ES'; // Establece el idioma en español castellano
    const voices = window.speechSynthesis.getVoices();
    //voz de mujer argentina = 239, de hombre= 240
    utterance.voice = voices[239];
    utterance.rate = 1.1; // 0.1 to 10
    console.log(utterance);
    const voice = voices.find(voice => voice.name.includes('Spanish Spain Male'));
    if (voice) {
      utterance.voice = voice;
    }
    speechSynthesis.speak(utterance);
  }
  isGenderMale(description: string) {
    // get first word
    const words = description.split(' ');
    const firstWord = words[0];
    firstWord.charAt(firstWord.length - 1);
    return firstWord.includes('a') || firstWord.includes('u') || firstWord.includes('o');
  }


  private speakIfProductsFound(products: Product[], data: ProductData) {
    this.products = products;
    console.log(data);
    console.log('entra a speack');
    let textToSpeack = this.authService.getName() + ", No se encontraron resultados para " + data.description;
    data.brand ? textToSpeack += ' de marca ' + data.brand : '';
    console.log(products.length);
    if (this.products.length > 1) {
      console.log(this.getProductsDescription(data));
      this.speak(this.getProductsDescription(data));
    } else if (this.products.length === 1) {
      const product = <Product>this.products.pop();
      const genere = this.isGenderMale(product.description) ? 'El ' : 'La ';
      textToSpeack = genere + product.description + ' de marca ' + product.brand.brand
        + ' cuesta ' + this.getCost(product.salePrice) + ' pesos'  ; /!*+  ' por ' + product.unitOfMeasurement?.measure *!/
      this.speak(textToSpeack);
    } else {
      this.speak(textToSpeack)
    }
    console.log(textToSpeack);
  }

  private getCost(salePrice: number): string {
    console.log(salePrice);
    if (salePrice.toString().length === 1) {
      return salePrice.toString();
    } else if (salePrice.toString().length === 2) {
      return salePrice.toString();
    } else if (salePrice.toString().length === 3) {
      const cent = Number(salePrice.toString().substring(0, 1)) * 100;
      const decena = Number(salePrice.toString().substring(1, 2)) * 10;
      const unit: number = Number(salePrice.toString().substring(2, 3));
      return cent.toString() + ' ' + this.validateIfIsZero(decena).toString() + ' ' + this.validateIfIsZeroForUnit(unit).toString();
    } else if (salePrice.toString().length === 4) {
      const oneHundred = Number(salePrice.toString().substring(0, 1)) * 1000;
      const cent = Number(salePrice.toString().substring(1, 2)) * 100;
      const decena: number = Number(salePrice.toString().substring(2, 3)) * 10;
      const unidad: number = Number(salePrice.toString().substring(3, 4));
      return oneHundred.toString() + ' ' +
        this.validateIfIsZero(cent).toString() + ' ' +
        this.validateIfIsZero(decena).toString() + ' ' +
        this.validateIfIsZeroForUnit(unidad).toString();
    }
    else if (salePrice.toString().length === 5) {
      console.log('es aca');
      const unidadDe10000 = this.getNumberFor10000(Number(salePrice.toString().substring(0, 2)));
      const centena: number = Number(salePrice.toString().substring(2, 3))* 100;
      const decena: number = Number(salePrice.toString().substring(3, 4)) * 10;
      const unidad: number = Number(salePrice.toString().substring(4, 5));

      return unidadDe10000.toString() + ' ' +
        this.validateIfIsZero(centena).toString() + ' ' +
        this.validateIfIsZero(decena).toString() + ' ' +
        this.validateIfIsZeroForUnit(unidad).toString();
    }
    else if (salePrice.toString().length === 6) {
      const unidadDeMillon = Number(salePrice.toString().substring(0, 1)) * 100000;
      const unidadDe10000 = Number(salePrice.toString().substring(1, 2)) * 10000;
      const unidadDe1000: number = Number(salePrice.toString().substring(2, 3)) * 1000;
      const centena: number = Number(salePrice.toString().substring(3, 4)) * 100;
      const decena: number = Number(salePrice.toString().substring(3, 4)) * 10;
      const unidad: number = Number(salePrice.toString().substring(3, 4));
      return unidadDeMillon.toString() + ' ' +
        unidadDe10000.toString() + ' ' +
        this.validateIfIsZero(unidadDe1000).toString() + ' ' +
        this.validateIfIsZero(centena).toString() + ' ' +
        this.validateIfIsZero(decena).toString() + ' ' +
        this.validateIfIsZeroForUnit(unidad).toString();
    }
    return salePrice.toString();
  }
  private validateIfIsZero(value: number) {
    return value === 0 ? '' : value;
  }
  private validateIfIsZeroForUnit(value: number) {
    return value === 0 ? '' : 'y '  + value;
  }
  private getProductsDescription(data: ProductData) {
    const textToSpeack = "encontre " + this.products.length + "resultados para " + data.description + ', ';
    let textToSpeakConcat = '';
    this.products.forEach((product: Product) =>{
      const description = product.description;
      const brand =   ' de marca ' + product.brand.brand.toLowerCase() + ' cuesta ';
      textToSpeakConcat += description + brand + this.getCost(product.salePrice).toString() + ' pesos , ';
    });
    console.log(textToSpeack);
    console.log(textToSpeakConcat);
    return textToSpeack + textToSpeakConcat;
  }

  verifyRecognitionState() {
    console.log('entro');
    const res = localStorage.getItem('searchByVoice');
    if(res === null){
      localStorage.setItem('searchByVoice', 'false');
      this.recognitionIsEnable = false;
    }
    else if(res === 'true'){
      this.recognitionIsEnable = true;
      this.startRecognition();
      this.speak("");
    }
  }

  private getNumberFor10000(unit10000: number) {
    console.log(unit10000);
    return unit10000 + " mil" ;
  }
}
*/
