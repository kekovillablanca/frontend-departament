import { Injectable } from '@angular/core';

import {KeycloakService} from "keycloak-angular";
import {KeycloakTokenParsed} from "keycloak-js";
@Injectable({

  providedIn: 'root'
})
export class AuthService {
  constructor(
              private keycloakAuthService: KeycloakService,
              ) {
  }
  login(username: string, password: string): Promise<void> {
    return this.keycloakAuthService.login();
  }
  getToken(): KeycloakTokenParsed{
    return <KeycloakTokenParsed>this.keycloakAuthService.getKeycloakInstance()?.idTokenParsed;
  }

  public register(email: string, password: string, role: string):Promise<any> {
    return this.keycloakAuthService.register();
  }

  getLoggedUser(): KeycloakTokenParsed {

    return <KeycloakTokenParsed>this.keycloakAuthService.getKeycloakInstance().idTokenParsed;
  }

  getEmailFromLoggedUser(): string {
    let keycloakTokenParsed: KeycloakTokenParsed | undefined = this.keycloakAuthService.getKeycloakInstance()?.idTokenParsed;
    return keycloakTokenParsed?.['email'];
  }
  getUsernameFromLoggedUser(): string {
    return this.keycloakAuthService.getUsername();
  }
  logOut() {
    this.keycloakAuthService.logout().then(r => {

    });
  }
  private getGroupsFromUser(): void{
    this.keycloakAuthService.getToken().then(token => {
    });
  }


  verifyPattern(valor: string): boolean {
    // Definimos la expresión regular que coincide con el patrón
    const regex = /^sale-system-[a-zA-Z0-9]+-employee$/;
    // Verificamos si el valor coincide con la expresión regular
    return regex.test(valor);
  }


}
