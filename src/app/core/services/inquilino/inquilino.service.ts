import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, catchError, map, throwError } from 'rxjs';
import { Renter } from '../../domain/Renter';
import {RentersEndpoint} from "../../config/api";
import {CommandService} from "../../command/command.service";
import {QueryService} from "../../query/query.service";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Injectable({
  providedIn: 'root'
})
export class InquilinoService implements  CommandService<Renter>,QueryService<Renter>{
  constructor(private httpClient: HttpClient) {

  }

  public findAll(): Observable<Renter[]> {
    return this.httpClient.get<Renter[]>(RentersEndpoint.get).pipe(
      map(renters =>
        renters.map(r => new Renter(r.id, r.name, r.lastname,r.dni,r.salary,r.phone))

      )
    )
  }

  public update(Inquilino: Renter): Observable<Renter> {
    return this.httpClient.put<Renter>(RentersEndpoint.get + '/' + Inquilino.id, Inquilino);

  }
  public save(Inquilino: Renter): Observable<Renter> {
    return this.httpClient.post<Renter>(RentersEndpoint.post, Inquilino).pipe(catchError(error => {
      return throwError("la Renter no existe..", error);
    }));

  }
  public delete(inquilino: Renter): Observable<Renter> {
    console.log(inquilino.id);
    return this.httpClient.delete<Renter>(RentersEndpoint.post+ '/' + inquilino.id).pipe();

  }





}
//new Renter(p.id, p.nombre,p.dni, p.apellido )
