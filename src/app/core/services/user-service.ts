import { Observable } from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {QueryService} from "../query/query.service";
import {User} from "../domain/user";
import {CommandService} from "../command/command.service";
@Injectable({
  providedIn: 'root'
})
export class UserService implements QueryService<User>,CommandService<User>{
  constructor(private http: HttpClient) {
  }

  save(entity: User): Observable<User> {
        throw new Error("Method not implemented.");
    }
    delete(entity: User): Observable<User> {
        throw new Error("Method not implemented.");
    }

  findAll(): Observable<User[]> {
        throw new Error("Method not implemented.");
    }

  filterBy(criteria: any): Observable<User[]> {
        throw new Error("Method not implemented.");
    }

  all(): Observable<User[]> {
    return this.http.get<User[]>("");
  }

  create(user: User): Observable<User> {
    return this.http.post<User>("userEndpoint.url", user);
  }



  get(id: string): Observable<User> {
    return this.http.get<User>("userEndpoint.url" + `/${id}`);

  }

  update(user: User): Observable<User> {
    return this.http.put<User>("userEndpoint.url" + `/${user.id}`, user);
  }

  page(page: number, size: number): Observable<any> {
    return this.http.get<any>("userEndpoint.url" + `/page?numberPage=${page}&numberOfRows=${size}`);
  }

  login(user: User):Observable<User> {
    return this.http.post<User>("userEndpoint.url", user);
  }



}
