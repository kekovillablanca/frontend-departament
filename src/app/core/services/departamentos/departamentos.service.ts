import { Injectable } from '@angular/core';

import { Renter } from '../../domain/Renter';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, throwError, map } from 'rxjs';
import { Departament } from '../../domain/Departament';

import { GenericResponse } from '../../domain/generic-response';
import { DocumentoAlmacenado } from '../../domain/documento-almacenado';
import { DomSanitizer } from '@angular/platform-browser';
import {CommandService} from "../../command/command.service";
import {DepartamentEndpoint} from "../../config/api";


@Injectable({
  providedIn: 'root'
})
export class DepartamentosService{




  constructor(private httpClient: HttpClient,private sanitizer: DomSanitizer) {

  }
  public findAll():Observable<Departament[]>{
    //pipe se utiliza para encadenar operadores que transforman, filtran o combinan los datos emitidos por un observable.
    //mas que nada se le pasa un observable en el cual a este pipe se le asigna el observable
    //que seria departamento recorre esta matriz en el primer map
    //y en el segundo ya el objeto
    return this.httpClient.get<Departament[]>(DepartamentEndpoint.get).pipe(
      map(departament=>departament.map(d=> {

        console.log(departament);
          return new Departament(d.id,d.number,d.address,d.departmentState,d.renter);


      })))
  }
  public save(departament: Departament): Observable<Departament> {
    return this.httpClient.post<Departament>(DepartamentEndpoint.get, departament).pipe(catchError(error => {
      return throwError("el departamento no existe..", error);
    }));

}
  /*
  guardarImagen(formData: FormData): Observable<GenericResponse<DocumentoAlmacenado>> {
    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    return this.httpClient.post<GenericResponse<DocumentoAlmacenado>>(this.resourceUrlDocumento, formData, {headers}).pipe(
      map(response => new GenericResponse(response)));
  }

  public find(id: number): string {
    const baseUrl = `${this.resourceUrlDocumento}/`;
    const uniqueTimestamp = new Date().getTime(); // Valor de tiempo actual como número único
    return `${baseUrl}${id}?timestamp=${uniqueTimestamp}`;
          }
  public getImagenUrl(fileName: number): string {
            const baseUrl = `${this.resourceUrlDocumento}/download/`;
            const uniqueTimestamp = new Date().getTime(); // Valor de tiempo actual como número único
            return `${baseUrl}${fileName}?timestamp=${uniqueTimestamp}`;
          }

  public findAllDocumento(): Observable<GenericResponse<DocumentoAlmacenado[]>> {
    return this.httpClient.get<GenericResponse<DocumentoAlmacenado>>(this.resourceUrlDocumento).pipe(
      map((response=>new GenericResponse(response))))
  }*/

}
