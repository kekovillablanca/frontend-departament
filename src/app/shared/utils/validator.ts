import {Injectable} from "@angular/core";

@Injectable({providedIn: 'root'})
export class Validator{
  public isValid(valueToValidate: any) {
    return valueToValidate !== null && valueToValidate !== undefined && valueToValidate !== '' && valueToValidate.trim().length !== 0;
  }
}
