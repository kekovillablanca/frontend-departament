import {Injectable, } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class RoleFormatter{
    public formatRole(role: string): string{
        switch(role) {
            case "ROLE_ADMIN":
              return "Administrador";
            case "ROLE_EMPLOYEE":
              return "Empleado";
            default:
              return "";
          }

    }

}

