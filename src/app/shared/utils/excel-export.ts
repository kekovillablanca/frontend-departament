import {Injectable} from "@angular/core";
import {NumberFormatter} from "./number-formatter";
import {DatePipe} from "@angular/common";
import {Product} from "../../../models/product";
import FileSaver from "file-saver";

@Injectable({
  providedIn: 'root',
})
export class ExcelExport{
  constructor() {
  }
  public exportIncome(incomes: any[]): void{
    import('xlsx').then(xlsx => {
      let incomesToXlsx: any[] = [];
      const header :any [] = ['Monto','Cuenta','Razon social', 'Cuil/cuit', 'Observacion','Fecha de creacion','Dependencia de origen', 'Partida presupuestaria','Estado']
      incomes.forEach( (p) => {
        incomesToXlsx.push({
          'Monto': ExcelExport.addDots(p.amount.toString()),
          'Cuenta': p.account,
          'Razon social': p.businessName,
          'Cuil/cuit': p.cuilCuit,
          'Observacion': p.observation,
          'Fecha de creación':  ExcelExport.changeFormatDate(p.creationDate),
          'Dependencia de origen': p.origin == null? 'No asignado' : p.origin.name,
          'Partida presupuestaria' : p.budgetItem,
        });
      });
      const worksheet = xlsx.utils.json_to_sheet(incomesToXlsx);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "Ingresos");
    });
  }

  private static changeFormatDate(dateToChange: Date): string{
    let pipe = new DatePipe('en-US');
    return <string>pipe.transform(dateToChange, "dd/MM/yyyy");
}
  private static addDots(value: string): string {
    return new NumberFormatter().addDots(value.toString());
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE

    });
    FileSaver.saveAs(data, fileName  + ' ' + new Date().toLocaleString() + EXCEL_EXTENSION);
  }

  exportProduct(products: Product[]) {

  }
}
