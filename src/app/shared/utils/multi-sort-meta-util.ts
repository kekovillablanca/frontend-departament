import {SortMeta} from "primeng/api/sortmeta";
import {Injectable} from "@angular/core";
import {Criteria, Sort} from "../../core/models/criteria";
@Injectable({
  providedIn: 'root'
})
export class MultiSortMetaUtil {
  public generateCriteria(multiSortMeta: any, numberPage: number, numberOfRows: number): Criteria {
    const criteria = new Criteria();
    criteria.orders = multiSortMeta.map((r: SortMeta) => {
      return new Sort(r.field, r.order === 1 ? 'ASC' : 'DESC');
    });
    criteria.page = numberPage;
    criteria.size = numberOfRows;
    return criteria;
  }

}
