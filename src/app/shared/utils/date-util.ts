import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class DateUtil{
  public typeOfFilterDate(date: string): string{
    switch (date) {
      case 'dateIs': return 'DATE_IS';
      case 'dateAfter': return 'DATE_IS_AFTER';
      case 'dateBefore': return 'DATE_IS_BEFORE';
      case 'dateIsNot': return 'DATE_IS_NOT';
      default: return 'equals';
    }

  }

}
