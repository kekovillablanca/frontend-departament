import {Injectable, OnInit} from "@angular/core";
import { Component } from "@angular/core";
@Component({
    template: ''
  })
@Injectable({
  providedIn: 'root',
})
export class NumberFormatter{
    public addDots(number: string): string{
        const index = number.lastIndexOf('.');
        let beforeComma = '';
        let afterComma = '';
        let dottedBeforeComma ='';
        if (index !== -1) {
            beforeComma = number.slice(0, index);
            afterComma = number.slice(index + 1);

            dottedBeforeComma = beforeComma.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

            return dottedBeforeComma +","+afterComma;
        }

        return number.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }

}

