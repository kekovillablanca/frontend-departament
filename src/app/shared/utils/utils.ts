export class Util {

  /**
   * Método de apoyo para los constructores. Genera nuevos objetos Date si los campos tienen formato fecha.
   * @param target Objeto destino DTO
   * @param parameters JSON
   */
  public static createInstanceWithParameters(target: any, parameters: any): any {

    if (parameters) {

      Object.assign(target, parameters);

      Object.keys(target).forEach((fieldName: string) => {

        if (parameters.fieldName && typeof parameters.fieldName === 'string') {
          const splitDate = parameters.fieldName.split('/');
          if (splitDate.length === 3) {
            target[fieldName] = new Date(Number(splitDate[2]), Number(splitDate[1]), Number(splitDate[0]));
          }
        }
      });
    }

    return target;
  }
}
