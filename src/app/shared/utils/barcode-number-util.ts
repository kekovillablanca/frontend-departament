import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class BarcodeNumberUtil {
  generate(): string {
    const numeroDeCodigo = this.generateBarcodeNumber();
    const checksum = this.calculateChecksum(numeroDeCodigo);
    return numeroDeCodigo + checksum;
  }
   generateBarcodeNumber(): string {
    let num = '';
    for (let i = 0; i < 12; i++) {
      num += Math.floor(Math.random() * 10).toString();
    }
    return num;
  }

   calculateChecksum(num: string): number {
    let sum = 0;
    for (let i = 0; i < num.length; i++) {
      let multiplier = (i % 2 === 0) ? 1 : 3;
      sum += parseInt(num.charAt(i)) * multiplier;
    }
     return (10 - (sum % 10)) % 10;
  }

}
