import {Injectable} from "@angular/core";
import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";
import * as XLSX from "xlsx";
import FileSaver from "file-saver";

interface Column {
  field: string;
  header: string;
  customExportHeader?: string;
}

interface ExportColumn {
  title: string;
  dataKey: string;
}

@Injectable({
  providedIn: 'root'
})
export class Export{
constructor() {

}

  exportPdf({rows, exportColumns}: { rows: any, exportColumns: any },title:string){
    const doc = new jsPDF('p', 'pt');
    doc.setFont("Georgia");
    doc.setFontSize(40);
    autoTable(doc, {
      columns: exportColumns,
      body: rows,
      styles: { overflow: 'linebreak', fontSize: 7, font: 'helvetica' },
      /*didParseCell: function (data) {
        console.log(data.cell.text.includes("producto"));
        if (data.cell.text[0].includes("producto")) {

          data.cell.styles.font="Georgia";
        }
      },*/
    });
    doc.save(title+".pdf");

  }


  exportExcel({rows, exportColumns}: { rows: any, exportColumns: any },title:string) {

        const wb = XLSX.utils.book_new();
        const ws = XLSX.utils.json_to_sheet(rows);

        XLSX.utils.book_append_sheet(wb, ws, title);

        XLSX.writeFile(wb, title+".xlsx");


  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }


}
