import { Injectable } from "@angular/core";
import { DatePipe } from "@angular/common";
@Injectable({
  providedIn: 'root',
})
export class DateFormatter{
  public formatDate(date: Date ):string {
    let pipe = new DatePipe('en-US');
    return <string>pipe.transform(date, "dd/MM/yyyy");
  }
}
