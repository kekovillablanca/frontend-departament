
export const navigation = {
    home : 'home',
    pages : 'pages',
    login : 'login',
    users : 'users',
    products: 'products',
    addresses : 'addresses',
    departments : 'departments',
    list : 'list',
    register : 'register',
    providers : 'providers',
    brands : 'brands',
    renters : 'renters',
    purchases : 'purchases',
    update: 'update',
    paramList: 'list/:title/:message',
    id: '/:id',
    search: "savvy-search",
    report: "report",
  endorsements: "endorsements"


}

export const userNavigation = {
  list: navigation.pages + '/' + navigation.users + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.users + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.users + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.users + '/' + navigation.update ,
}

export const productNavigation = {
  list: navigation.pages + '/' + navigation.products + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.products + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.products + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.products + '/' + navigation.update ,
}

export const saleNavigation = {
  list: navigation.pages + '/' + navigation.addresses + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.addresses + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.addresses + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.addresses + '/' + navigation.update,
}
export const productTypeNavigation = {
  list: navigation.pages + '/' + navigation.departments + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.departments + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.departments + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.departments + '/' + navigation.update,
}
export const loginNavigation = {
  login: navigation.login,
}
export const providerNavigation = {
    list: navigation.pages + '/' + navigation.providers + '/' + navigation.list,
    paramList: navigation.pages + '/' + navigation.providers + '/' + navigation.paramList,
    register: navigation.pages + '/' + navigation.providers + '/' + navigation.register,
    update: navigation.pages + '/' + navigation.providers + '/' + navigation.update,

}
export const brandNavigation = {
  list: navigation.pages + '/' + navigation.brands + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.brands + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.brands + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.brands + '/' + navigation.update,
}
export const noteNavigation = {
  list: navigation.pages + '/' + navigation.renters + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.renters + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.renters + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.renters + '/' + navigation.update,
}
export const purchaseNavigation = {
  list: navigation.pages + '/' + navigation.purchases + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.purchases + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.purchases + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.purchases + '/' + navigation.update,
}

export const endorsementNavigation = {
  list: navigation.pages + '/' + navigation.endorsements + '/' + navigation.list,
  paramList: navigation.pages + '/' + navigation.endorsements + '/' + navigation.paramList,
  register: navigation.pages + '/' + navigation.endorsements + '/' + navigation.register,
  update: navigation.pages + '/' + navigation.endorsements + '/' + navigation.update,
}
