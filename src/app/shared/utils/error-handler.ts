import {ErrorHandler, NgModule} from "@angular/core";
import {ConfirmationService, MessageService} from "primeng/api";

export class MyErrorHandler implements ErrorHandler {
    private readonly error: any;
    constructor(error: string, messageService: MessageService) {
        this.error = error;
        messageService.add({severity:'error', summary:'Error', detail: this.error});
    }
    handleError(error: any): void {
    }

}


