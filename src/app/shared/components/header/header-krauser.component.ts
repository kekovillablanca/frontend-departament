import { Component, OnInit } from '@angular/core';
import {MenuDetailUsersComponent} from "../menu-detail-users/menu-detail-users.component";

@Component({
  selector: 'app-header-krauser',
  templateUrl: './header-krauser.component.html',
  standalone: true,
  imports: [
    MenuDetailUsersComponent
  ],
  styleUrls: ['./header-krauser.component.css']
})
export class HeaderKrauserComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }


}
