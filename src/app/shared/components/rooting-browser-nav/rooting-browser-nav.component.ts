import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ConfirmationService, MenuItem, MessageService} from "primeng/api";
import {MatSidenav} from "@angular/material/sidenav";
import {MenubarModule} from "primeng/menubar";
import {FooterComponent} from "../footer/footer.component";
import {MenuDetailUsersComponent} from "../menu-detail-users/menu-detail-users.component";
import {NgOptimizedImage} from "@angular/common";
import {AuthService} from "../../../core/services/auth-service";
@Component({
  selector: 'app-rooting-browser-nav',
  templateUrl: './rooting-browser-nav.component.html',
  styleUrls: ['./rooting-browser-nav.component.css'],
  providers: [MessageService, ConfirmationService],
  imports: [
    MenubarModule,
    FooterComponent,
    MenuDetailUsersComponent,
    NgOptimizedImage
  ],
  standalone: true
})

export class RootingBrowserNavComponent implements OnInit {
  public routes: MenuItem[] = [];
    // @ts-ignore
  @ViewChild('sidenav') sidenav: MatSidenav ;
  private isLogged: boolean = true;
  constructor(private authService: AuthService, private router: Router) {
  }
  ngOnInit(): void {
    this.routes = this.generateRoutes();
    this.addIndexToRoutes();
  }
  private addIndexToRoutes() {
    this.routes.forEach((r,i)=>{r.tabindex = i.toString()});
  }

  private generateRoutes(): MenuItem[] {
    return [
      {
        label: 'File',
        icon: 'pi pi-fw pi-file',
        routerLink: ['/pages/addresses']
      },
      {
        label: 'Edit',
        icon: 'pi pi-fw pi-pencil',
        routerLink: ['/pages/departments']
      },
      {
        label: 'Users',
        routerLink: ['/pages/users'],
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'New',
            icon: 'pi pi-fw pi-user-plus'
          },
          {
            label: 'Delete',
            icon: 'pi pi-fw pi-user-minus'
          },
          {
            label: 'Search',
            icon: 'pi pi-fw pi-users',
            items: [
              {
                label: 'Filter',
                icon: 'pi pi-fw pi-filter',
                items: [
                  {
                    label: 'Print',
                    icon: 'pi pi-fw pi-print'
                  }
                ]
              },
              {
                icon: 'pi pi-fw pi-bars',
                label: 'List'
              }
            ]
          }
        ]
      },
      {
        label: 'Inquilino',
        icon: 'pi pi-fw pi-calendar',
        routerLink:"/pages/renters"
      },
      {

        label: this.isLogged ? "Deslogearse" : "Logearse",
        icon: "pi pi-user",
        command: () => {
        }
      },

    ];
  }


}
