import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RootingBrowserNavComponent } from './rooting-browser-nav.component';

describe('RootingBrowserNavComponent', () => {
  let component: RootingBrowserNavComponent;
  let fixture: ComponentFixture<RootingBrowserNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RootingBrowserNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RootingBrowserNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
