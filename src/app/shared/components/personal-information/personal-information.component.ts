import {Component, Inject, OnInit} from '@angular/core';
import {MatButtonModule} from "@angular/material/button";
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogTitle
} from "@angular/material/dialog";
import {CheckboxModule} from "primeng/checkbox";
import {FormsModule} from "@angular/forms";
import {MatLabel} from "@angular/material/form-field";
import {ButtonModule} from "primeng/button";
import {RippleModule} from "primeng/ripple";
import {AuthService} from "../../../core/services/auth-service";
import {environment} from "../../../../environments/environment";
class UserInformation{
  name: string = '';
  lastname: string = '';
  username: string = '';
  email: string = '';
  role: string = '';
  verifiedEmail: boolean = false;
  document: string = '';
  constructor() {
  }
}
@Component({
  selector: 'app-personal-information',
  standalone: true,
  imports: [
    MatButtonModule,
    MatDialogActions,
    MatDialogClose,
    MatDialogContent,
    MatDialogTitle,
    CheckboxModule,
    FormsModule,
    MatLabel,
    ButtonModule,
    RippleModule
  ],
  templateUrl: './personal-information.component.html',
  styleUrl: './personal-information.component.css'
})

export class PersonalInformationComponent implements OnInit {
  displayModal: boolean = false;
  user: UserInformation;
  isSearchByVoiceEnable!: boolean;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private AuthService: AuthService) {
   this.user = new UserInformation();
  }
  ngOnInit(): void {
    this.assignUserInformation();
    this.displayModal = true;
    this.isSearchByVoiceEnable = localStorage.getItem('searchByVoice') === 'true';
  }
  close() {
    this.displayModal = false;
  }
  modifySearchByVoice() {
  }

  returnToAdministrationAccount() {
    window.open(environment.keycloak.url + '/realms/' + environment.keycloak.realm + '/account/')
  }

  private assignUserInformation() {
    const loggedUser = this.AuthService.getLoggedUser();
    this.user.name = loggedUser['given_name'];
    this.user.lastname = loggedUser[`family_name`];
    this.user.username = loggedUser[`preferred_username`];
    this.user.email = loggedUser[`email`];
    this.user.role = loggedUser[`groups`][0];
    this.user.verifiedEmail = loggedUser[`email_verified`];
    this.user.document = loggedUser[`document`];
  }


}
