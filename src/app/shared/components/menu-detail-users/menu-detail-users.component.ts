import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {PersonalInformationComponent} from "../personal-information/personal-information.component";
import {
  MatAccordion,
  MatExpansionPanel,
  MatExpansionPanelDescription,
  MatExpansionPanelHeader
} from "@angular/material/expansion";
import {MatIcon} from "@angular/material/icon";
import {ButtonModule} from "primeng/button";
import {SidebarModule} from "primeng/sidebar";
import {MatButton} from "@angular/material/button";
import {AuthService} from "../../../core/services/auth-service";

@Component({
  selector: 'app-menu-detail-users',
  templateUrl: './menu-detail-users.component.html',
  styleUrls: ['./menu-detail-users.component.css'],
  imports: [
    MatAccordion,
    MatExpansionPanel,
    MatExpansionPanelHeader,
    MatIcon,
    MatExpansionPanelDescription,
    ButtonModule,
    SidebarModule,
    MatButton
  ],
  standalone: true,

})
export class MenuDetailUsersComponent implements OnInit {
  protected username: string = "";
  isMobile = false;
  isVisibleSidebar = false;
  constructor(private authService: AuthService, private route: Router,public dialog: MatDialog,private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver.observe([Breakpoints.Handset]).subscribe(result => {
      this.isMobile = result.matches;
    });
  }

  ngOnInit(): void {
    console.log('entrasdsado');
    this.username = this.authService.getUsernameFromLoggedUser();
  }

  logout() {
    this.authService.logOut();
  }

  showDialogPersonalInformation() {
   this.dialog.open(PersonalInformationComponent, {
      data: {
        displayModal: true,
      },
      width: '50%',
      height: 'auto',
    });
  }


}
