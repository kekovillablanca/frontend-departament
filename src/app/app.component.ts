import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import {RouterOutlet} from "@angular/router";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  imports: [
    RouterOutlet
  ],
  standalone: true
})
export class AppComponent {
  title = 'keycloak-frontend';
  logearse:boolean=false;
  token:string="";

  constructor(private primengConfig: PrimeNGConfig) {}






}
