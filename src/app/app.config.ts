import {ApplicationConfig, importProvidersFrom} from '@angular/core';
import {PreloadAllModules, provideRouter, withHashLocation, withPreloading} from '@angular/router';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import {HttpClient, HttpClientModule, provideHttpClient} from "@angular/common/http";
import {PrimeNGConfig} from "primeng/api";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {provideClientHydration} from "@angular/platform-browser";
import {BrowserAnimationsModule, provideAnimations} from "@angular/platform-browser/animations";
import {DOCUMENT} from "@angular/common";
import {KeycloakBearerInterceptor, KeycloakService} from "keycloak-angular";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {primeNgTranslation} from "./core/config/prime-ng-translation";
import {routes} from "./app.routes";
import {HttpLoaderFactory, KeycloakBearerInterceptorProvider, KeycloakInitializerProvider} from './core/config/init/app-inits';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideAnimationsAsync(),
    provideHttpClient(),
    importProvidersFrom(HttpClientModule, KeycloakBearerInterceptor,BrowserAnimationsModule),
    {provide: Window, useValue: window},
    {provide: PrimeNGConfig, useValue: primeNgTranslation()},
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
    KeycloakService,
    provideRouter(routes, withPreloading(PreloadAllModules),withHashLocation()),
    provideClientHydration(),
    provideAnimations(),
    KeycloakInitializerProvider,
    KeycloakBearerInterceptorProvider,
    { provide: Document, useExisting: DOCUMENT },
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }).providers!

  ]
};


